@extends('template.default')
@section('page-title', 'Oops -- emmards')
@section('content')
<div class="text-center top-venti">
    <div>We're working on it.</div>
    <div class="bottom-grande">我々はそれに取り組んでいます。待ってね！</div>
    <div class="bottom-venti">
        <img src="{{URL('/assets/images/under-construction.gif')}}" title="under construction" alt="under construction" style="max-width: 100%;" />
        <div>gif taken from <a href="https://giphy.com/gifs/adventure-time-cooking-bacon-kkQfnuqlSifgQ" rel="nofollow" target="_blank">giphy</a></div>
    </div>
    <div>Under construction.</div>    
</div>
@stop