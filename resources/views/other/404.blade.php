@extends('template.default')
@section('page-title', 'Oops -- emmards')
@section('content')
<div class="text-center top-venti">
    <div style="font-size: 3em;">Oops!</div>
    <div class="bottom-grande" style="font-size: 1.5em;">ごめんなさい！</div>
    <div>We don't have what you are looking for.</div>
    <div class="bottom-grande">そのページはありません。</div>
    <div class="bottom-venti">
        <img src="{{URL('/assets/images/sorry.gif')}}" title="under construction" alt="under construction" style="max-width: 100%;" />
        <div>gif taken from <a href="https://tenor.com/view/ppkk08-sorry-adventure-time-jake-gif-8025376" rel="nofollow" target="_blank">tenor</a></div>
    </div>
    <div>404 not found.</div>
</div>
@stop