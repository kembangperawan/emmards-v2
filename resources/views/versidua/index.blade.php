<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('page-title')</title>
    <meta title="@yield('page-title')">
    <meta name="description" content="Experienced Product Manager with a demonstrated history of working in the B2C and B2B retail industry. Skilled in analysis, UML Design, Front End Development and UI/UX. Strong in technical and product management professional with a Bachelor focused in Information Technology.">
    <meta name="robots" content="INDEX, FOLLOW, NOODP, NOYDIR">
    <meta name="keywords" content="Product Manager, Web Developer">
    <meta property="og:image" content="{{URL('/assets/images/about-me-banner.JPG')}}">

    <link rel="icon" href="{{URL('/assets/images/favicon.png')}}" type="image/x-icon">

    <!-- Bootstrap -->
    <link href="{{URL('/assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- font awesome -->
    <link href="{{URL('/assets/font-awesome/css/fontawesome-all.min.css')}}" rel="stylesheet">
    <!-- <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script> -->
    <link href="https://fonts.googleapis.com/css?family=Dosis:300,400,600,700" rel="stylesheet">
    <link href="{{URL('/assets/bootstrap/css/snh.css?v=3')}}" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('head-script')
  </head>
  <body>
  <nav class="navbar navbar-default navbar-fixed-top no-border" id="navbar">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand brand" href="{{URL('/')}}"><span style="color: #007eff;">E</span>STORIES</a>
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right main-menu">
          <li><a href="#"><i class="fa fa-home"></i></a></li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="container">
        <div class="col-xs-12">
            <ul class="posts">
            @for($i=0;$i<10;$i++)
                <li>
                    <div class="header">
                        <span class="date">THU, 14 FEB 2019</span>
                    </div>
                    <h2><a href="#">This is {{ $i+1 }} title</a></h2>
                    <p>This is {{ $i+1 }} description.</p>
                    <div class="tag-container">
                        <i class="fas fa-tag"></i>
                        @for($j=0;$j<5;$j++)
                        <a class="tag" href="{{URL('/tag/1')}}">Tag {{$j}}</a>
                        @endfor
                    </div>
                </li>
            @endfor
            </ul>
        </div>
  </div>
    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{URL('/assets/bootstrap/js/bootstrap.min.js')}}"></script>

  </body>
</html>