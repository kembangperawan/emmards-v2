@extends('template.default')
@section('page-title', 'Portfolio -- emmards')
@section('content')
<input type="hidden" id="active-menu" value="work" />
<div style="margin-bottom: 40px;">
    <h1>My Works</h1>
    <p>Trust me, I done these works.</p>
</div>
<div class="portfolio-tag text-center">
    <ul class="list-inline">
        <li class="active"><a href="#">All</a></li>
        <li><a href="#">Web</a></li>
        <li><a href="#">Mobile Apps</a></li>
        <li><a href="#">Other</a></li>
    </ul>
</div>
<div class="row portfolio">
    @for($i=0;$i<18;$i++)
    <div class="col-md-6 col-xs-12 item">
        <div class="inner">
            <div class="image-container">
                <img src="{{URL('/assets/images/sample-post.jpg')}}" alt="Photo #{{$i}}" title="Photo #{{$i}}" />
            </div>
            <div class="foot">
                <a class="detail-link" href="{{URL('/portfolio/'.$i)}}">View Detail</a>
                <h3>This is my portfolio # {{$i + 1}} hahssdnnasd adasdansdas dasdas dnasdnasd jasdas</h3>
                <div class="date">Feb 2017</div>
            </div>
        </div>
    </div>
    @endfor
</div>
@stop