<li class="nav-item"  data-name="about"><a class="nav-link" href="{{URL('/about-me')}}">About me</a></li>
<li class="nav-item" data-name="work"><a class="nav-link" href="{{URL('/work')}}">Works</a></li>
<li class="nav-item" data-name="gallery"><a class="nav-link" href="{{URL('/gallery')}}">Galleries</a></li>
<li class="nav-item" data-name="post"><a class="nav-link" href="{{URL('/story')}}">Stories</a></li>