<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('page-title')</title>
    <meta title="@yield('page-title', 'emmards -- software engineer')">
    <meta name="description" content="@yield('meta-description', 'Experienced Product Manager with a demonstrated history of working in the B2C and B2B retail industry. Skilled in analysis, UML Design, Front End Development and UI/UX. Strong in technical and product management professional with a Bachelor focused in Information Technology.')">
    <meta name="robots" content="INDEX, FOLLOW, NOODP, NOYDIR">
    <meta name="keywords" content="@yield('meta-tag', 'Product Manager, Web Developer')">
    <meta property="og:image" content="@yield('meta-image', URL('/assets/images/about-me-banner.JPG'))">

    <link rel="icon" href="{{URL('/assets/images/favicon.png')}}" type="image/x-icon">

    <!-- Bootstrap -->
    <link href="{{URL('/assets/bootstrap4/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- font awesome -->
    <link href="{{URL('/assets/font-awesome/css/fontawesome-all.min.css')}}" rel="stylesheet">
    <!-- <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script> -->
    <link href="https://fonts.googleapis.com/css?family=Dosis:300,400,600,700" rel="stylesheet">
    <link href="{{URL('/assets/css/snh.css?v=2')}}" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('head-script')
  </head>
  <body>
  <nav class="navbar navbar-expand-lg fixed-top navbar-light bg-white" id="navbar">
    <div class="container">
      <a class="navbar-brand" href="{{URL('/')}}">EMMARDS</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto" id="main-menu">
        @include('include.menu')
      </ul>
    </div>
  </nav>
    
    <div class="container">
      <div class="row">
        <div class="col-12">
          @yield('content')
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          @yield('fluid-content')
        </div>
      </div>
    </div>

    <div class="container back-to-top top-venti" id="move-to-top">Back to Top</div>
    <!-- footer -->
    <footer>
      <div class="container footer">
        <div class="row">
          <div class="col-md-6 col-12 social">
            <ul class="list-inline">
              <li class="list-inline-item"><a href="mailto:surabi.eman@gmail.com" rel="nofollow"><i class="far fa-envelope"></i></a></li>
              <li class="list-inline-item"><a class="instagram" href="https://www.instagram.com/emmards/" rel="nofollow" target="_blank"><i class="fab fa-instagram"></i></a></li>
              <li class="list-inline-item"><a class="twitter" href="https://www.instagram.com/emmards/" rel="nofollow" target="_blank"><i class="fab fa-twitter"></i></a></li>
              <li class="list-inline-item"><a class="linkedin" href="https://www.instagram.com/emmards/" rel="nofollow" target="_blank"><i class="fab fa-linkedin"></i></a></li>
            </ul>
          </div>
          <div class="col-md-6 col-12 text-right">&copy; <a href="{{URL('/')}}">emmards</a> 2018-2019</div>
        </div>
      </div>
    </footer>
    <!-- end footer -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{URL('/assets/bootstrap4/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript">

    var profile = $("#profile");
    var sidenav = $("#main-menu");
    var navbar = $("#navbar");
    var sidenavs = $(".navbar-nav");
    var activeMenu = $("#active-menu");
    $(document).scroll(function() {
      if($(document).scrollTop() > 5) {
        navbar.removeClass("no-border");
      } else {
        navbar.addClass("no-border");
      }
    });
    $(document).ready(function() {
      if(activeMenu.length) {
        for(var i=0;i<sidenavs.length;i++) {
          var children = sidenavs.children();
          for(var j=0;j<children.length;j++) {
            if ($(children[j]).attr("data-name") == activeMenu.val()) {
              $(children[j]).addClass("active");
            }
          }
        }
      }
    });
    $("#move-to-top").click(function() {
      $("html, body").animate({ scrollTop: 0 }, "slow");
      return false;
    });
    </script>

  @if (CONFIG('app.server_location') != 'local')
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-68688180-2"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-68688180-2');
  </script>
  @endif
  @yield('script')
  </body>
</html>