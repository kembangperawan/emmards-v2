@extends('template.default')
@section('page-title', 'Archive -- emmards')
@section('content')
<input type="hidden" id="active-menu" value="archive" />
<h1>Archive</h1>
<div class="row" style="margin-bottom: 10px;">
    <div class="col-xs-4">
    <select class="form-control">
        <option>All</option>
        @foreach(config('month') as $key => $value)
            <option>{{$value['en']}}</option>
        @endforeach    
        </select>
    </div>
</div>
    
<ul class="posts">
@for($i=0;$i<100;$i++)
    <li>
        <a href="{{URL('/post/'.$i)}}">
            <h2>Post {{$i}}</h2>
            <span class="date">17 Dec 1991</span>
        </a>
    </li>
@endfor
</ul>
@stop