@extends('template.default')
@section('page-title', 'Posts -- emmards')
@section('head-script')
<link href="{{URL('/assets/bootstrap/css/snh-story.min.css')}}" rel="stylesheet">
@stop
@section('content')
<input type="hidden" id="active-menu" value="story" />
<div style="margin-bottom: 40px;">
    <h1>My Post</h1>
    <p>You might see blurred photos, undefined videos or unnecessary content. But they are from my slices of life.</p>
</div>
<ul class="posts">
@foreach($blogs as $key => $value)
    <li>
        <div class="header">
            <span class="date">{{$value->get_formatted_timestamp()}}</span>&nbsp;&nbsp;
        </div>
        <h2><a href="{{URL($value->link())}}">{{$value->title}}</a></h2>
        <p>{{ $value->meta_description }}</p>
        <div class="read-container">
            <a class="read" href="{{URL($value->link())}}">read</a>
        </div>
        <div class="tag-container">
            <i class="fas fa-tag"></i>&nbsp;&nbsp;
            @foreach($value->tag_list() as $tagKey => $tagValue)
            <a class="tag" href="{{URL('/tag/'.$tagValue)}}">{{$tagValue}}</a>
            @endforeach
        </div>
    </li>
@endforeach
</ul>
<div class="row">
    <div class="col-xs-12">{{ $blogs->links() }}</div>
</div>
@stop