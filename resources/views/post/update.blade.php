@extends('template.default')
@section('page-title', 'Post Update -- emmards')
@section('head-script')

<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
@stop
@section('content')
<input type="hidden" id="active-menu" value="post" />
<h1>Post Create</h1>
<form method="post" action="{{ url(isset($blog) ? '/post-manage/' . $blog->id :'/post-manage') }}" onsubmit="return submit_content()" accept-charset="UTF-8">
    <input name="_method" type="hidden" value="{{ isset($blog) ? 'PUT': 'POST' }}" />
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
        <label>Title</label>
        <input type="text" name="title" class="form-control" value="{{ isset($blog) ? $blog->title : old('title') }}" />
    </div>
    <div class="form-group">
        <label>Status</label>
        <select name="status" class="form-control">
        @foreach(config('post-config')['status'] as $key => $value)
            <option value="{{$value}}"{{ isset($blog) && $blog->status == $value ? 'selected=selected' : '' }}>{{$value}}</option>
        @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Content</label>
        <textarea name="content_id" class="form-control" id="content_id" >{{ isset($blog) ? $blog->content_id : old('content_id') }}</textarea>
        <div id="editor">
            <p>Hello World!</p>
        </div>
    </div>
    <div class="form-group">
        <label>Tag</label>
        <input type="text" name="tag" class="form-control" value="{{ isset($blog) ? $blog->tag : old('tag') }}" />
    </div>
    <div class="form-group">
        <label>Meta Tag</label>
        <input type="text" name="meta_tag" class="form-control" value="{{ isset($blog) ? $blog->meta_tag : old('meta_tag') }}" />
    </div>
    <div class="form-group">
        <label>Meta Image</label>
        <input type="text" name="meta_image" class="form-control" value="{{ isset($blog) ? $blog->meta_image : old('meta_image') }}" />
    </div>
    <div class="form-group">
        <label>Meta Description</label>
        <textarea name="meta_description" class="form-control">{{ isset($blog) ? $blog->meta_description : old('meta_description') }}</textarea>
    </div>
    <div class="form-group">
        <label>Published Date</label>
        <input type="text" name="published_date" class="form-control" value="{{ isset($blog) ? $blog->published_date : old('published_date') }}" />
        <span class="small">format : YYYY-MM-dd hh:mm:ss</span>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button class="btn btn-default">Cancel</button>
        <button class="btn btn-danger">Delete</button>
    </div>
</form>
@stop
@section('script')

<!-- Main Quill library -->
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script type="text/javascript">
    var toolbarOptions = [
  ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
  ['blockquote', 'code-block'],

  [{ 'header': 1 }, { 'header': 2 }],               // custom button values
  [{ 'list': 'ordered'}, { 'list': 'bullet' }],
  [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
  [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
  [{ 'direction': 'rtl' }],                         // text direction

  [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
  [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

  [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
  [{ 'font': [] }],
  [{ 'align': [] }],
  [ 'link', 'image', 'video', 'formula' ],

  ['clean']                                         // remove formatting button
];
        var quill = new Quill('#editor', {
            modules: {
    toolbar: toolbarOptions
  },
    theme: 'snow'
  });

  function submit_content() {
      console.log( quill.root.innerHTML);
      $('#content_id').val(quill.root.innerHTML);
      return true;
  }
</script>
@stop