@extends('template.default')
@section('page-title', $blog->title.' -- emmards')
@section('head-script')
<link href="{{URL('/assets/bootstrap/css/snh-story.css')}}" rel="stylesheet">
@stop
@section('content')
<input type="hidden" id="active-menu" value="story" />
<h1>{{$blog->title}}</h1>
@if(false)
<ul class="language pull-right">
    @foreach(config('language') as $key => $value)
    <li {{$value['code'] == 'EN' ? 'class=active' : ''}}><a href="#">{{$value['code']}}</a></li>
    @endforeach
</ul>
@endif
<div class="timestamp">{{$blog->get_formatted_timestamp()}}</div>
<div class="tag-container bottom-grande">
    <i class="fas fa-tag"></i>&nbsp;&nbsp;
    @foreach($blog->tag_list() as $tagKey => $tagValue)
    <a class="tag" href="{{URL('/tag/'.$tagValue)}}">{{$tagValue}}</a>
    @endforeach
</div>

<div class="post-detail bottom-venti">
{!! $blog->get_content() !!}
</div>
<div class="row bottom-venti">
    <div class="col-md-4 col-xs-12">
        @if(!is_null($older))
        <a href="{{ URL($older->link()) }}">
            Older
            <h3>{{ $older->title }}</h3>
        </a>
        @endif
    </div>
    <div class="col-md-4 col-md-offset-4 col-xs-12 text-right">
        @if(!is_null($newer))
        <a href="{{ URL($newer->link()) }}">
            Newer
            <p>{{ $newer->title }}</p>
        </a>
        @endif
    </div>
    <div class="col-xs-12">
        <a class="btn btn-block" href="{{ URL('/story') }}">View All Posts</a>
    </div>
</div>

@stop