@extends('template.default')
@section('page-title', 'Title -- emmards')
@section('content')
<input type="hidden" id="active-menu" value="story" />
<h1>This is Just a Title</h1>
<ul class="language pull-right">
    @foreach(config('language') as $key => $value)
    <li {{$value['code'] == 'EN' ? 'class=active' : ''}}><a href="#">{{$value['code']}}</a></li>
    @endforeach
</ul>
<div class="timestamp">Sun, 25 Feb 2018</div>
<div class="post-detail">
<p>
    In est do nostrud quis. Irure <a href="#">this is link</a> anim fugiat et consequat ipsum dolore esse ex minim non cupidatat nisi nulla. Aute sunt eu laboris sit enim. Dolore ipsum elit cillum quis laboris aliqua amet laborum fugiat nulla occaecat.
</p>
<p>
<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/BfSARhUl0-O/" data-instgrm-version="8" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BfSARhUl0-O/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">マスクライダー . . #mn_snap #vsco #explorebandung</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A post shared by <a href="https://www.instagram.com/emmards/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px;" target="_blank"> emmanuel sitinjak</a> (@emmards) on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2018-02-17T02:38:11+00:00">Feb 16, 2018 at 6:38pm PST</time></p></div></blockquote> <script async defer src="//www.instagram.com/embed.js"></script>
</p>
<p>
    Proident exercitation cillum laboris sunt do est qui. Consectetur enim consectetur pariatur laboris aliquip duis id. Anim culpa consequat qui occaecat duis incididunt proident commodo qui qui excepteur deserunt ipsum eiusmod. Enim nulla amet tempor cillum exercitation deserunt deserunt exercitation proident. Non elit eu ad ipsum dolor excepteur laborum reprehenderit cupidatat minim.
</p>
<img src="http://emmanuels-mbp/snh/assets/images/sample-post.jpg" alt="title" title="title" />
<div class="caption">Shirakawago 2017</div>

<p>
    Anim cillum aliquip mollit incididunt sint. Non laborum duis nulla sit quis esse mollit aliqua incididunt ex ad mollit aute. Et ad commodo amet labore culpa est labore Lorem quis voluptate fugiat. Incididunt aliquip sunt minim irure laboris amet do reprehenderit duis. Sint exercitation nulla velit laboris velit amet.
</p>
<p>
<blockquote class="twitter-tweet" data-lang="en"><p lang="tr" dir="ltr">Ngiler turbular 🤤</p>&mdash; em (@emmards) <a href="https://twitter.com/emmards/status/943076221746782208?ref_src=twsrc%5Etfw">December 19, 2017</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
<p>
    Mollit non eiusmod voluptate laboris duis. Eu velit ipsum aliquip incididunt ex nisi laboris fugiat dolore aute. Eu irure culpa sit do irure laborum ut magna veniam veniam eiusmod ex eu exercitation. Veniam reprehenderit sint ea et laborum mollit adipisicing in Lorem eiusmod anim.
</p>

</div>
@stop
