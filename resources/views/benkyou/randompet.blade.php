@extends('benkyou.template')
@section('page-title', ' benkyou ')
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="text-center"><img src="{{URL('/assets/images/benkyo/'.$image)}}" style="max-height: 500px;" alt="pet" title="pet" /></div>
        <h1 id="answer" style="visibility: hidden;" class="text-center">{{ $image }}</h1>
        <div class="text-center">
            <button class="btn btn-default btn-lg" onClick="show()">Show</button>
            <a href="{{URL('/benkyou/randompet')}}" class="btn btn-primary btn-lg">New</a>
        </div>
    </div>
</div>
@stop

@section('script')
<script type="text/javascript">
    function show(){
        $('#answer').removeAttr('style');
    }
</script>
@stop


