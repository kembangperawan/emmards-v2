@extends('benkyou.template')
@section('page-title', ' benkyou ')
@section('content')
<div class="row">
    <div>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            @foreach($tabs as $value)
            <li role="presentation"><a href="#{{$value}}" aria-controls="{{$value}}" role="tab" data-toggle="tab">{{$value}}</a></li>
            @endforeach
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            @foreach($tabs as $value)
            <div role="tabpanel" class="tab-pane" id="{{$value}}">
            <div class="row">
                @foreach($items as $key => $item)
                    @if($value == $item['category'])
                    <div class="col-xs-3"><button class="btn btn-default btn-lg btn-block">{{$item['name']}}</button></div>  
                    @endif
                @endforeach
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@stop

@section('script')
<script type="text/javascript">
    function show(){
        $('#answer').removeAttr('style');
    }
</script>
@stop


