@extends('benkyou.template')
@section('page-title', ' benkyou ')
@section('content')
<div class="row">
    <div class="col-xs-12">
        <h1>{{ $number }}</h1>
        <p id="answer" style="visibility: hidden">{{ $answer }}</p>
        <button class="btn btn-default" onClick="show()">Show</button>
        <a href="{{URL('/benkyou/number')}}" class="btn btn-primary">New</a>
    </div>
</div>
@stop

@section('script')
<script type="text/javascript">
    function show(){
        $('#answer').removeAttr('style');
    }
</script>
@stop


