@extends('template.default')
@section('page-title', 'Gallery -- emmards')
@section('content')
<input type="hidden" id="active-menu" value="gallery" />
<div class="bottom-venti">
    <h1>Gallery</h1>
    <p>Captured by me.</p>
</div>
<div class="row gallery">
    @if ((CONFIG('app.server_location') == 'local'))
    <div class="col-xs-12 bottom-tall">
        <a href="{{URL('/gallery-manage/create')}}" class="btn btn-default">Add New</a>
    </div>
    @endif
    <div class="col-xs-12">
        <ul class="list-inline text-center bottom-venti gallery-selected-camera">
        <li><a href="{{URL('/gallery')}}">All</a></li>
        @foreach(CONFIG('gallery-config.camera') as $key => $value)
            @if ($value['value'] == $selected_camera)
            <li><span>{{$value['value']}}</span></li>
            @else
            <li><a href="{{URL('/camera/'.$value['canonical'])}}">{{$value['value']}}</a></li>
            @endif
        @endforeach
        </ul>
    </div>
   @foreach($image_galleries as $key => $value)
   <div class="col-md-4 col-sm-6 col-xs-12 item">
       <a class="img-thumbnail" href="{{URL($value->link())}}">
            <img src="{{$value->image_url}}" alt="{!! $value->caption !!}" title="{!! $value->caption !!}" {{$value->orientation == 'PORTRAIT' ? 'class=portrait' : ''}} />
       </a>
       <p class="caption">
            {!! (CONFIG('app.server_location') == 'local') && $value->status == 'UNPUBLISHED' ? '<span class="bg-danger">(UNPUBLISHED)</span>' : '' !!} {!! $value->caption !!}
        </p>
   </div>
   @endforeach
   <div class="col-xs-12 bottom-tall top-tall text-center">
        {{  $image_galleries->appends(Request::except('page'))->render() }}
        <p>Total Result {{  $image_galleries->total() }}</p>
        <p>Page {{  $image_galleries->currentPage() }} of {{  $image_galleries->lastPage() }}</p>
   </div>
</div>

@stop


