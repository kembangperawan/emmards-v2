@extends('template.default')
@section('page-title', $image_gallery->caption.' -- emmards')
@section('content')
<input type="hidden" id="active-menu" value="gallery" />
<div class="bottom-venti">
    <h1>Gallery</h1>
    <p>Captured by me.</p>
</div>
<div class="row gallery">
   <div class="col-xs-12 detail bottom-grande">
       <img src="{{$image_gallery->image_url}}" title="{!! $image_gallery->caption !!}" alt="{!! $image_gallery->caption !!}" />
   </div>
   @if ((CONFIG('app.server_location') == 'local'))
    <div class="col-xs-12 bottom-tall">
        <a href="{{URL('/gallery-manage/'.$image_gallery->id.'/edit')}}" class="btn btn-default">Edit</a>
    </div>
    @endif
   <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
        <h3>{!! $image_gallery->caption !!}</h3>
        <p class="bottom-tall">{{$image_gallery->get_formatted_taken()}}</p>
        <!-- camera info -->
        <p><i class="fas fa-camera"></i>&nbsp;&nbsp;{{$image_gallery->camera}}</p>
        @if(count($image_gallery->exif()) > 0)
        @foreach($image_gallery->exif() as $key => $value)
        <address class="bottom-tall">
            <strong>{{$value->label}}</strong><br>
            {{$value->value}}
        </address>
        @endforeach
        @endif
   </div>
</div>
   <div class="col-xs-12">
        <nav aria-label="...">
            <ul class="pager">
                @if(isset($previous) && $previous != null)
                <li class="previous"><a href="{{URL($previous->link())}}"><span aria-hidden="true">&larr;</span> Older</a></li>
                @endif
                @if(isset($next) && $next != null)
                <li class="next"><a href="{{URL($next->link())}}">Newer <span aria-hidden="true">&rarr;</span></a></li>
                @endif
            </ul>
        </nav>
   </div>
</div>

@stop

@section('script')
<!-- <script type="text/javascript">
    $(document).ready(function() {
    $("#main-container").addClass("fluid");
    });
</script> -->
@stop


