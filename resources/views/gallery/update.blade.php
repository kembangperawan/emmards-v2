@extends('template.default')
@section('page-title', 'Gallery Update -- emmards')
@section('content')
<input type="hidden" id="active-menu" value="post" />
<h1>Gallery Create</h1>
<form method="post" action="{{ url(isset($image_gallery) ? '/gallery-manage/' . $image_gallery->id :'/gallery-manage') }}" accept-charset="UTF-8">
    <input name="_method" type="hidden" value="{{ isset($image_gallery) ? 'PUT': 'POST' }}" />
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
        <label>flickr ID</label>
        <input type="text" name="flickr_id" class="form-control" value="{{ isset($image_gallery) ? $image_gallery->flickr_id : old('flickr_id') }}" />
    </div>
    <div class="form-group">
        <label>URL</label>
        <input type="text" name="image_url" class="form-control" value="{{ isset($image_gallery) ? $image_gallery->image_url : old('image_url') }}" />
    </div>
    <div class="form-group">
        <label>Status</label>
        <select name="status" class="form-control">
        @foreach(config('gallery-config')['status'] as $key => $value)
            <option value="{{$value}}"{{ isset($image_gallery) && $image_gallery->status == $value ? 'selected=selected' : '' }}>{{$value}}</option>
        @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Orientation</label>
        <select name="orientation" class="form-control">
        @foreach(config('gallery-config')['orientation'] as $key => $value)
            <option value="{{$value}}"{{ isset($image_gallery) && $image_gallery->orientation == $value ? 'selected=selected' : '' }}>{{$value}}</option>
        @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Caption</label>
        <input type="text" name="caption" class="form-control" value="{{ isset($image_gallery) ? $image_gallery->caption : old('caption') }}" />
    </div>
    <div class="form-group">
        <label>Tags</label>
        <input type="text" name="tags" class="form-control" value="{{ isset($image_gallery) ? $image_gallery->tags : old('tags') }}" />
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button class="btn btn-default">Cancel</button>
        <button class="btn btn-danger">Delete</button>
    </div>
</form>
@stop