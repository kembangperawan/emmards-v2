<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>emmanuel_cv</title>

    <!-- Bootstrap -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    body {
        color: #3D3D3D;
        font-family: 'Roboto', sans-serif;
        font-size: 12px;
        margin: 0;
        padding: 20px;
    }
    .bt{
        border-top: 1px solid #000;
    }
    .btd{
        border-top: 2px solid #000;
    }
    .br{
        border-right: 1px solid #000;
    }
    .brd{
        border-right: 2px solid #000;
    }
    .bb{
        border-bottom: 1px solid #000;
    }
    .bb-dot{
        border-bottom: 1px dot #000;
    }
    .bbd{
        border-bottom: 2px solid #000;
    }
    table.left-border tr:not(:first-child) td:first-child {
        border-left: 2px solid #000;
    }
    table.left-border tr:last-child td{
        border-bottom: 2px solid #000;
    }
    table tr td{
        border-bottom: 1px solid #000;
    }
    table tr td.dot{
        border-bottom: 1px dotted #000;
    }
    .nb {
        border-width: 0 !important;
    }
    .nb-bottom {
        border-bottom-width: 0 !important;
    }
    .right {
        text-align: right;
    }
    </style>
  </head>
  <body>
    <table class="left-border" style="width: 100%; margin-bottom: 20px;" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3" class="nb">履歴書</td>
        </tr>
        <tr>
            <td class="nb right" colspan="3">平成30年1月10日 現在</td>
            <td  class="nb" rowspan="5"></td>
        </tr>
        <tr>
            <td class="btd dot">ふ り が な</td>
            <td class="btd dot">ハムザ マウラナ</td>
            <td class="brd btd">男・女</td>
        </tr>
        <tr>
            <td class="nb-bottom" colspan="2">氏 名</td>
            <td class="brd" rowspan="2">印</td>
        </tr>
        <tr>
            <td class="brd" colspan="2">Emmanuel Ardianto Sitinjak</td>
        </tr>
        <tr>
            <td>生 年 月 日</td>
            <td class="brd" colspan="2">生 年 月 日生 年 月 日</td>
        </tr>
        <tr>
            <td>ふ り が な</td>
            <td colspan="2">ふ り が なふ り が なふ り が な</td>
            <td class="btd brd">電話: 070-4104-5928</td>
        </tr>
        <tr>
            <td>現 住 所</td>
            <td colspan="2">生 年 月 日生 年 月 日</td>
            <td class="brd" rowspan="2">E-Mail: maulhamzah@gmail.com</td>
        </tr>
        <tr>
            <td colspan="3">東京都調布市深大寺北町4丁目24番地1 三鷹ビレッジ5</td>
        </tr>
        <tr>
            <td>ふ り が な</td>
            <td colspan="2">ふ り が なふ り が なふ り が な</td>
            <td class="brd" rowspan="2">電話: </td>
        </tr>
        <tr>
            <td>現 住 所</td>
            <td colspan="2">生 年 月 日生 年 月 日</td>
        </tr>
        <tr>
            <td width="15%"></td>
            <td width="50%"></td>
            <td width="10%"></td>
            <td width="25%"></td>
        </tr>
    </table>
    <!-- experiences -->
    <table style="width: 100%;" border="1">
        <tr>
            <td width="15%"></td>
            <td width="5%"></td>
            <td width="80%"></td>
        </tr>
        <tr>
            <td>年</td>
            <td>月</td>
            <td>学歴・職歴(各別にまとめて書く)</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>学 歴</td>
        </tr>
        <tr>
            <td>2007年</td>
            <td>3</td>
            <td>SMA67 高校 入学 (インドネシア)</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>職歴</td>
        </tr>
        <tr>
            <td>2007年</td>
            <td>3</td>
            <td>SMA67 高校 入学 (インドネシア)</td>
        </tr>
    </table>
    <!--  -->
    <table style="width: 100%;" border="1">
        <tr>
            <td width="15%"></td>
            <td width="5%"></td>
            <td width="80%"></td>
        </tr>
        <tr>
            <td>年</td>
            <td>月</td>
            <td>学歴・職歴(各別にまとめて書く)</td>
        </tr>
        @for($i=0;$i<5;$i++)
        <tr>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
        </tr>
        @endfor
    </table>

    <!--  non formal -->
    <table style="width: 100%;" border="1">
        <tr>
            <td width="15%"></td>
            <td width="5%"></td>
            <td width="80%"></td>
        </tr>
        <tr>
            <td>年</td>
            <td>月</td>
            <td>資格・語学</td>
        </tr>
        <tr>
            <td>2007年</td>
            <td>3</td>
            <td>SMA67 高校 入学 (インドネシア)</td>
        </tr>
        @for($i=0;$i<3;$i++)
        <tr>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
        </tr>
        @endfor
    </table>
    <!-- jiko shoukai -->
    <table style="width: 100%;" border="1">
        <tr>
            <td>自己PR/趣味/特技など</td>
        </tr>
        <tr>
            <td>御社で大規模にITサポート に携わり、ITのを通して、多くの人の暮らしをさせたいと考え、御社を希望しており
ます。大学時代に、情報工学で学んだ知識を活かして、父の企業で簡単な社内に向けパソコンを作った経験
があります。社内の人は仕事のためいつも自分のノートパソコンを会社に持ちますのでその問題を解決するの
は自作パソコンを作ること。そして社内の人にパソコンの関係の問題が起きって私は一生懸命サポートしまし
た。自作パソコンを作るのは簡単なものでしたが、社内の人から感謝され、ハードウエアとトラブルシューティン
グが持つ力に活かして社会を支えていきたいと考えるようになりました。貴社でなら、システムを通して、より多
くの人々の生活を支え、効率化していけるのではないかと考え、貴社を強く志望しております。</td>
        </tr>
    </table>
    <!-- jiko shoukai -->
    <table style="width: 100%;" border="1">
        <tr>
            <td>希望記入欄(特に給料・職種・勤務時間・勤務地・その他についての希望などがあれば記入)</td>
        </tr>
        <tr>
            <td></td>
        </tr>
    </table>
  </body>
</html>