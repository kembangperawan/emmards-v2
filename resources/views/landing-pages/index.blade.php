@extends('template.default')
@section('page-title', 'emmards -- Software Engineer')
@section('content')
<div class="row">
    @if(false)
    <div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3 bottom-venti">
        <div class="text-center"><b>Hi</b>, welcome to my website;</div>
        <div class="profile">
            <img src="{{URL('/assets/images/thumbnail.JPG')}}" alt="profile" title="emmanuel" />
            <div class="name">I'm emmanuel</div>
            <div class="description">An ordinary person who want to speak 5 languages fluently.</div>
            <br />
            <p>Lets get closer!</p>
            <div class="social">
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="mailto:surabi.eman@gmail.com" rel="nofollow"><i class="far fa-envelope"></i></a></li>
                    <li class="list-inline-item"><a class="instagram" href="https://www.instagram.com/emmards/" rel="nofollow" target="_blank"><i class="fab fa-instagram"></i></a></li>
                    <li class="list-inline-item"><a class="twitter" href="https://twitter.com/emmards" rel="nofollow" target="_blank"><i class="fab fa-twitter"></i></a></li>
                    <li class="list-inline-item"><a class="linkedin" href="https://www.linkedin.com/in/emmanuel-ardianto-703b1a67/" rel="nofollow" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-xs-12 bottom-venti">
        <h2 class="bottom-grande">Currently I am</h2>
        <div class="row">
            @foreach(config('about-me.front-activity') as $key => $value)
            <div class="col-md-4 col-xs-12">
            <h3>{{$key}}</h3>
                @foreach($value as $subkey => $subvalue)
                <p><a href="{{$subvalue['url']}}" rel="nofollow" target="_blank" title="{{$subvalue['name']}}">{{$subvalue['name']}}</a></p>
                @endforeach
            </div>
            @endforeach
        </div>
    </div>
    @endif
    <div class="col-xs-12 bottom-venti top-grande">
        <div class="row gallery">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="text-center"><b>Hi</b>, welcome to my website;</div>
                <div class="profile">
                    <img src="{{URL('/assets/images/thumbnail.JPG')}}" alt="profile" title="emmanuel" />
                    <div class="name">I'm emmanuel</div>
                    <div class="description">An ordinary person who want to speak 5 languages fluently.</div>
                    <br />
                    <div class="social">
                        <ul class="list-inline">
                            <li class="list-inline-item"><a href="mailto:surabi.eman@gmail.com" rel="nofollow"><i class="far fa-envelope"></i></a></li>
                            <li class="list-inline-item"><a class="instagram" href="https://www.instagram.com/emmards/" rel="nofollow" target="_blank"><i class="fab fa-instagram"></i></a></li>
                            <li class="list-inline-item"><a class="twitter" href="https://twitter.com/emmards" rel="nofollow" target="_blank"><i class="fab fa-twitter"></i></a></li>
                            <li class="list-inline-item"><a class="linkedin" href="https://www.linkedin.com/in/emmanuel-ardianto-703b1a67/" rel="nofollow" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            @foreach($image_galleries as $key => $value)
            <div class="col-md-4 col-sm-6 col-xs-12 item">
                <a class="img-thumbnail" href="{{URL($value->link())}}">
                        <img src="{{$value->image_url}}" alt="{{$value->caption}}" title="{{$value->caption}}" {{$value->orientation == 'PORTRAIT' ? 'class=portrait' : ''}} />
                </a>
                <p class="caption">
                    {{$value->caption}}
                </p>
            </div>
            @endforeach
            
        </div>
        <div class="top-tall"><a href="{{URL('/gallery')}}" class="btn btn-default btn-block">View All</a></div>
    </div>
</div>

@stop