<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>emmanuel_cv</title>

    <!-- Bootstrap -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    body {
        color: #3D3D3D;
        font-family: 'Roboto', sans-serif;
        font-size: 12px;
    }
    h1 {
        font-weight: 700;
        font-size: 1.7em;
        margin-bottom: 30px;
    }
    h2 {
        font-weight: 700;
        font-size: 1.2em;
        margin: 0;
        padding: 10px 0;
    }
    p {
        margin: 5px 0;
    }
    table tr td {
        line-height: 2em;
    }
    table tr td:first-child {
        /* text-align: right; */
        padding-right: 5%;
        /* font-weight: 700; */
        /* width: 30%; */
        vertical-align: top;
    }
    th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    table {
        border: 2px solid black;
        margin-bottom: 10px;
    }
    /* table tr td.header {
        text-align: left;
        border-left: 3px solid #0984e3;
        padding: 0 0 0 5px;
        line-height: 0.5em;
    } */
    ul {
        padding-left: 15px;
        margin: 0;
    }
    ul li {
        line-height: 1.7em;
    }
    </style>
  </head>
  <body>
  <div>
    <h2>履歴書</h2>
    <table cellpadding="10px" cellspacing="0" style="width: 100%;">
        <tr>
            <td width="80%">
                氏名
                <p style="font-size: 1.5em; text-align: center;">
                    エマニュエルアルディアントシチンジャク
                    <br />
                    <span style="font-size: 0.8em;">Emmanuel Ardianto Sitinjak</span>
                </p>
            </td>
            <td rowspan="2" style="text-align: center;"><img src="{{URL('/assets/images/pas.jpg')}}" style="height: 150px;" title="pas" alt="pas" /></td>
        </tr>
        <tr>
            <td>
                <p style="font-size: 1.5em; text-align: center;">
                    1991 <strong>年</strong> 12 <strong>月</strong> 17 <strong>日生</strong> (27歳)
                </p>
            </td>
        </tr>
        <tr>
            <td>
                住所
                <p style="font-size: 1.2em; text-align: center;">
                    1740076 東京板橋区上板橋三丁目１８番１０ー１０３
                </p>
            </td>
            <td>
                電話番号
                <p style="font-size: 1.3em; text-align: center;">
                    080 3503 2875
                </p>
            </td>
        </tr>
    </table>
    <table cellpadding="5px" cellspacing="0" style="width: 100%;">
        <tr>
            <td><p style="text-align: center; font-size: 1.2em; font-weight: bold;">雇用歴</p></td>
        </tr>
        <tr>
            <td>
                2018年 03月 -- 2016年 09月 <br />
                プロダクトマネージャー <br />
                Bhinneka.com
            </td>
        </tr>
        <tr>
            <td>
                2014年 03月 -- 2016年 08月 <br />
                ウェブエンジニア <br />
                Bhinneka.com
            </td>
        </tr>
    </table>
    <table cellpadding="5px" cellspacing="0" style="width: 100%;">
        <tr>
            <td><p style="text-align: center; font-size: 1.2em; font-weight: bold;">学歴</p></td>
        </tr>
        <tr>
            <td>
                2010 -- 2014<br />
                Bachelor <br />
                Faculty of Information Technology <br />
                Maranatha Christian University
            </td>
        </tr>
        <tr>
            <td>
                2007 -- 2010<br />
                高校 <br />
                Santa Maria 3 Cimahi <br />
            </td>
        </tr>
    </table>
</div>
</body>
</html>