@extends('template.default')
@section('page-title', 'About Me -- emmards')
@section('content')
<input type="hidden" id="active-menu" value="about" />
<div class="bottom-venti">
    <h1>About Me</h1>
    <p>You got me! Here I am;</p>
</div>
<div style="bottom-grande">
    <img src="{{URL('/assets/images/about-me-banner.JPG')}}" title="this is me" alt="this is me" style="width: 100%" />
</div>
<div>
    {!! $introduction !!}
</div>
<div class="text-center" style="margin: 30px 0;">
    <img src="{{URL('/assets/images/shark-dance.gif')}}" alt="Let's dancing!" title="Let's dancing" width="100px;" />
</div>
<!-- work experiences -->
<h2 class="bottom-grande">Work Experiences</h2>
<div class="row">
@foreach(config('about-me.work-experiences') as $key => $value)
    @if($value['visibility'])
        <div class="col-12 mb-4">
            <h3>{{$value['employer']}}</h3>
            <p class="card-subtitle mb-2 text-muted">{{$value['type_of_business']}}</p>
            <div class="row">
            @foreach($value['positions'] as $key => $positions)
                <div class="col-lg-6 col-md-6 col-12">
                    <div class="font-weight-bold">{{$positions['position']}}</div>
                    <div class="text-muted">{{$positions['date']}}</div>
                    <div class="font-weight-bold">Responsibilities</div>
                    <div class="mb-1">{{$positions['responsibilities']}}</div>
                    <div>
                        <a data-toggle="collapse" href="#{{ substr($positions['position'], 0, 3).$key }}" role="button" aria-expanded="false" aria-controls="collapseExample">
                            Project
                        </a>
                        </p>
                        <div class="collapse" id="{{ substr($positions['position'], 0, 3).$key }}">
                            <div class="card card-body">
                                <ul style="margin-bottom: 0;">       
                                    @foreach($positions['project'] as $subkey => $subvalue)
                                    <li>{{$subvalue}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    @endif
@endforeach
</div>
<!-- end work experience -->
<!-- education -->
<h2 class="bottom-grande">Education</h2>
<div class="row">
@foreach(config('about-me.education')['en'] as $key => $value)
    <div class="col-lg-6 col-md-6 col-12 mb-4">
        <h3>{{$value['institution']}}</h3>
        <p class="card-subtitle mb-2 text-muted">{{$value['date']}}</p>
        <div class="font-weight-bold">Grade</div>
        <div>{{$value['grade']}}</div>
        <div class="font-weight-bold">Field of study</div>
        <div>{{$value['field']}}</div>
    </div>
@endforeach
</div>
<!-- end education -->
<!-- skills -->
<h2 class="bottom-grande">Skills</h2>
<div class="row">
@foreach(config('about-me.skills') as $key => $value)
    <div class="col-lg-6 col-md-6 col-12 mb-4">
        <h3>{{$value['title']}}</h3>
        <ul>
        @foreach($value['items'] as $subkey => $subvalue)
            <li>{{$subvalue}}</li>
        @endforeach
        </ul>
    </div>
@endforeach
</div>
<!-- end skills -->
<!-- others -->
<h2 class="bottom-grande">Others</h2>
<dl class="row">
@foreach(config('about-me.others') as $key => $value)
  <dt class="col-lg-2 col-md-4 col-12">{{$value['key']}}</dt>
  <dd class="col-lg-10 col-md-8 col-12">{!! $value['value'] !!}</dd>
@endforeach
</dl>
<!-- end others -->
@stop