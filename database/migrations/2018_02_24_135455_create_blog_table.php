<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 500);
            $table->string('canonical', 500);
            $table->string('status', 20);
            $table->text('content_id');
            $table->text('content_en');
            $table->text('content_jp');
            $table->string('tag');
            $table->string('meta_tag', 500);
            $table->string('meta_description', 500);
            $table->string('meta_image', 500);
            $table->timestamp('published_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blogs');
    }
}
