<?php

if (! function_exists('toAscii')) {
    function toAscii($str, $replace=array(), $delimiter='-') {
        if( !empty($replace) ) {
         $str = str_replace((array)$replace, ' ', $str);
        }
       
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
       
        return $clean;
    }
}
if (! function_exists('getUUID')) {
    function getUUID($length = 20) {
        $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $string = '';
        $max = strlen($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, $max)];
        }
        return $string;
    }
}

if (! function_exists('isInternal')) {
    function isInternal() {
        if (ENV('APP_ENV') == 'local')
            return true;
        
        return false;
    }
}