<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Image_Gallery extends Model
{
    protected $table = 'image_galleries';

    // protected $casts = [
    //     'flickr_photo_exif' => 'array',
    // ];

    function get_canonical() {
        return toAscii($this->caption);
    }

    public function link() {
        return '/gallery/'.$this->canonical.'-'.$this->random_id;
    }

    public function get_formatted_timestamp() {
        $date = Carbon::parse($this->created_at);
        return $date->format('D, d M Y');
    }

    public function get_formatted_taken() {
        $photo_info = json_decode($this->flickr_photo_info_raw);
        if (count($photo_info) <= 0) {
            return '';
        }
        $taken = $photo_info->photo->dates->taken;
        $date = Carbon::parse($taken);
        return $date->format('D, d M Y');
    }

    public function exif() {
        return json_decode($this->flickr_photo_exif);
    }
}
