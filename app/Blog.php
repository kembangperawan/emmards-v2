<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Blog extends Model
{
    protected $table = 'blogs';

    public function tag_list() {
        $string = $this->tag;
        $split = explode(',', $string);
        return $split;
    }

    public function link() {
        return '/story/'.$this->canonical.'-'.$this->random_id;
    }

    public function get_content() {
        return $this->content_en;
    }

    public function get_formatted_timestamp() {
        $date = Carbon::parse($this->published_date);
        return $date->format('D, d M Y');
    }
}
