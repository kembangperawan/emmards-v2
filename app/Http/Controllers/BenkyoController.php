<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class BenkyoController extends Controller
{
    public function random_number() {
        $random = random_int(1,99999);
        // $random = 326910;
        $data = array(
            'number' =>  $random,
            'answer' => $this->spell($random)
        );
        return view('benkyou.number')->with($data);
    }
    public function spell($number) {

        switch($number) {
            case $number > 100000:
                $string = 'not available';
                break;
            case 1:
                $string =  'ichi';
                break;
            case 2:
                $string =  'ni';
                break;
            case 3:
                $string =  'san';
                break;
            case 4:
                $string =  'yon';
                break;
            case 5:
                $string =  'go';
                break;
            case 6:
                $string =  'roku';
                break;
            case 7:
                $string =  'nana';
                break;
            case 8:
                $string =  'hachi';
                break;
            case 9:
                $string =  'kyu';
                break;
            case 10:
                $string =  'jyu';
                break;
            case $number < 100:
                $prefix = '';
                if ($number >= 20) {
                    $prefix = $this->spell(floor($number / 10));
                }
                $suffix = $number % 10 > 0 ? '-'.$this->spell($number % 10) : '';
                
                $prefix .= 'jyu';
                
                $string = $prefix . $suffix;
                break;
            case $number < 1000:

                //get hyaku
                $prefix = 'hyaku';
                $hyaku = floor($number / 100) * 100;
                switch($hyaku / 100) {
                    case 1:
                        $prefix = 'hyaku';
                        break;
                    case 3:
                        $prefix = 'sambyaku';
                        break;
                    case 6:
                        $prefix = 'roppyaku';
                        break;
                    case 8:
                        $prefix = 'happyaku';
                        break;
                    default:
                        $prefix = $this->spell($hyaku / 100).$prefix;
                        break;
                }
                
                $jyu = $number - $hyaku;
                $prefix .= $jyu > 0 ? '-'. $this->spell($jyu) : '';
                
                $string = $prefix;
                break;
            case $number < 10000:

                //get sen
                $prefix = 'sen';
                $sen = floor($number / 1000) * 1000;
                switch($sen / 1000) {
                    case 1:
                        $prefix = 'sen';
                        break;
                    case 3:
                        $prefix = 'sanzen';
                        break;
                    case 8:
                        $prefix = 'hassen';
                        break;
                    default:
                        $prefix = $this->spell($sen / 1000).$prefix;
                        break;
                }
                
                $hyaku = $number - $sen;
                $prefix .=  $hyaku > 0 ? '-'. $this->spell($hyaku) : '';
                
                $string = $prefix;
                break;
            case $number < 100000:

                //get man
                $prefix = 'man';
                $man = floor($number / 10000) * 10000;
                switch($man / 10000) {
                    case 1:
                        $prefix = 'sen';
                        break;
                    default:
                        $prefix = $this->spell($man / 10000).$prefix;
                        break;
                }
                
                $sen = $number - $man;
                $prefix .= $sen > 0 ? '-'. $this->spell($sen) : '';
                
                $string = $prefix;
                break;
            default: 
                $string =  '';
                break;
        }
        return $string;
        
    }
    
    public function reji() {
        // print_r(CONFIG('reji'));
        // die();
        $tabs = ['ペット','フード', 'お菓子', '惣菜パン', '菓子パン', '場げ物'];
        $data = array(
            'tabs' => $tabs,
            'items' => CONFIG('reji.reji'),
            'images' => CONFIG('reji.image')
        );

        return view('benkyou.reji')->with($data);
    }

    public function randompet() {
        $images = CONFIG('reji.image');
        $random = random_int(0, count($images));
        $data = array(
            'image' => $images[$random]
        );
        return view('benkyou.randompet')->with($data);
        
    }

}
