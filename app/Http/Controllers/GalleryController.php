<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;;
use App\Image_Gallery;
use Goutte\Client;

class GalleryController extends Controller
{
    public function index() {
        // $html = file_get_contents('https://stackoverflow.com/questions/4423272/how-to-extract-links-and-titles-from-a-html-page');
        // $dom = new DOMDocument();
        // $dom->loadHTML($html);
        // $links = $dom->getElementsByTagName('a');
        

        // //Iterate over the extracted links and display their URLs
        // foreach ($links as $link){
        //     //Extract and show the "href" attribute.
        //     echo $link->nodeValue;
        //     echo $link->getAttribute('href'), '<br>';
        // }
        // die();
        // $client = new Client();
        // $crawler = $client->request('GET', 'https://www.yolo-japan.com/en/recruit/job/details/686');
        // $crawler->filter('div.job-details-mainarea-div')->each(function ($node) {
        //     print_r($node->text()."\n");
        // });

        // die();

        // camera
        $camera = isset($_GET['camera']) ? $_GET['camera'] : '';

        // location
        $location = isset($_GET['location']) ? $_GET['location'] : '';
        
        $image_galleries = $this->get_gallery($camera, $location);
        $data = array(
            'image_galleries' => $image_galleries,
            'selected_camera' => $camera
        );
        return view('gallery.index')->with($data);
    }

    public function cf_index_camera($canonical) {
        $camera = '';
        foreach(CONFIG('gallery-config.camera') as $key => $value) {
            if($value['canonical'] == $canonical)
                $camera = $value['value'];
        }
        $image_galleries = $this->get_gallery($camera, '');
        $data = array(
            'image_galleries' => $image_galleries,
            'selected_camera' => $camera
        );
        return view('gallery.index')->with($data);
    }

    public function cf_detail($canonical) {
        $random_id = substr($canonical, -20);
        $canonical = str_replace('-'.$random_id,'',$canonical);
        $image_gallery = Image_Gallery::where([
            ['canonical', '=', $canonical],
            ['random_id', '=', $random_id]
        ])->first();
        
        $previous = Image_Gallery::where('id', '<', $image_gallery->id)->orderBy('id','desc')->first();
        $next = Image_Gallery::where('id', '>', $image_gallery->id)->orderBy('id','asc')->first();
            
        $data = array(
            'image_gallery' => $image_gallery,
            'next' => $next,
            'previous' => $previous
        );
        return view('gallery.cf-detail')->with($data);
    }

    public function create() {
        return view('gallery.update');
    }

    public function edit($id) {
        $image_gallery = Image_Gallery::find($id);
        $data = array(
            'image_gallery' => $image_gallery
        );
        return view('gallery.update')->with($data);
    }

    public function store(Request $request) {
        $image_gallery = New Image_Gallery();
        $image_gallery->image_url = $this->get_flickr_img_url($request->get('image_url'));
        $image_gallery->flickr_id = $request->get('flickr_id');
        $image_gallery->status = $request->get('status');
        $image_gallery->orientation = $request->get('orientation');
        $image_gallery->caption = htmlspecialchars($request->get('caption'));
        $image_gallery->canonical = toAscii($image_gallery->caption);
        // $image_gallery->location = $request->get('location');
        // $image_gallery->longitude = $request->get('longitude');
        // $image_gallery->latitude = $request->get('latitude');
        $image_gallery->tags = $request->get('tags');
        $image_gallery->random_id = getUUID();

        // flickr exif

        //raw value
        $image_gallery->flickr_photo_info_raw = $this->get_flickr_photo_info($image_gallery->flickr_id);
        $flickr_photo_exif_raw = $this->get_flickr_exif_info($image_gallery->flickr_id);

        $exif_json = json_decode($flickr_photo_exif_raw);
        
        //get camera
        $image_gallery->camera = $exif_json->photo->camera;
        
        $exif_result = $this->get_flickr_exif($exif_json);
        $image_gallery->flickr_photo_exif_raw = json_encode($exif_result['raw']);
        $image_gallery->flickr_photo_exif = json_encode($exif_result['value']);


        $image_gallery->save();

        return redirect('/gallery-manage');
    }

    public function update(Request $request, $id)
    {
        $image_gallery = Image_Gallery::find($id);
        $image_gallery->image_url = $this->get_flickr_img_url($request->get('image_url'));
        $image_gallery->flickr_id = $request->get('flickr_id');
        $image_gallery->status = $request->get('status');
        $image_gallery->orientation = $request->get('orientation');
        $image_gallery->caption = htmlspecialchars($request->get('caption'));
        // $image_gallery->location = $request->get('location');
        // $image_gallery->longitude = $request->get('longitude');
        // $image_gallery->latitude = $request->get('latitude');
        $image_gallery->tags = $request->get('tags');

        // flickr exif

        //raw value
        $image_gallery->flickr_photo_info_raw = $this->get_flickr_photo_info($image_gallery->flickr_id);
        $flickr_photo_exif_raw = $this->get_flickr_exif_info($image_gallery->flickr_id);

        $exif_json = json_decode($flickr_photo_exif_raw);
        
        //get camera
        $image_gallery->camera = $exif_json->photo->camera;
        
        $exif_result = $this->get_flickr_exif($exif_json);
        $image_gallery->flickr_photo_exif_raw = json_encode($exif_result['raw']);
        $image_gallery->flickr_photo_exif = json_encode($exif_result['value']);


        $image_gallery->save();

        return redirect($image_gallery->link());
    }

    public function get_flickr_exif($exif) {
        $exif_array = array();
        foreach($exif->photo->exif as $key => $value) {
            $exif_obj = array(
                'key' => $value->tag,
                'label' => $value->label,
                'value' => $value->raw->_content
            );
            array_push($exif_array, $exif_obj);
        }

        $exif_array_shown = array();
        foreach(CONFIG('flickr.photo_exif') as $key => $value) {
            foreach($exif_array as $subkey => $subvalue) {
                if ($value == $subvalue['key']) {
                    array_push($exif_array_shown, $subvalue);
                    break;
                }
            }
        }
        $exif_return = array(
            'raw' => $exif_array,
            'value' => $exif_array_shown
        );
        return $exif_return;
    }

    public function get_flickr_photo_info($id) {
        return $this->get_flickr('flickr.photos.getInfo', $id, 'json');
    }

    public function get_flickr_exif_info($id) {
        return $this->get_flickr('flickr.photos.getExif', $id, 'json');
    }

    public function get_flickr($method = 'flickr.photos.getInfo', $id = '', $format = 'json') {
        $url = 'https://api.flickr.com/services/rest/?method='.$method;
        // add API KEY
        $url .= '&api_key='.CONFIG('flickr.app_key');
        // add API secret
        $url .= '&api_secret='.CONFIG('flickr.app_secret');
        // add photo id
        $url .= '&photo_id='.$id;
        //add format
        $url .= '&format='.$format;
        $url .= '&nojsoncallback=1';
        
        $json_result = file_get_contents($url);

        return $json_result;
    }

    public function test_canonical() {
        $string = 'Apple iPhone 7 Plus';
        return toAscii($string);
    }

    public function get_flickr_img_url($endcodedurl) {
        if (strlen($endcodedurl) < 250)
            return $endcodedurl;
        
        $decodedurl = urldecode($endcodedurl);
        $m = preg_match_all('/<img(.*?)src=("|\'|)(.*?)("|\'| )(.*?)>/s', $decodedurl, $matches);
        return $matches[3][0];
    }

    public function get_gallery($camera = '', $location = '') {

        $where_clause = array();
        $status = 'PUBLISHED';
        if (CONFIG('app.server_location') == 'local')
            $status = '';
        
        if ($status != '') 
            array_push($where_clause, ['status', '=', $status]);

        // camera
        foreach(CONFIG('gallery-config.camera') as $key => $value) {
            if($value['canonical'] == $camera)
                $camera = $value['value'];
        }

        if ($camera != '')
            array_push($where_clause, ['camera', '=', $camera]);

        if ($location != '')
            array_push($where_clause, ['location', '=', $location]);
        
        $image_galleries = Image_Gallery::where($where_clause)->orderBy('created_at', 'desc')->paginate(12);

        return $image_galleries;
    }
}
