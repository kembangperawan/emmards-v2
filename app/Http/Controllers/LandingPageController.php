<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Blog;
use App\Image_Gallery;

class LandingPageController extends Controller
{
    public function index() {
        // $blogs = Blog::orderBy('published_date', 'desc')->where('status', 'PUBLISHED')->take(3)->get();
        $image_galleries = Image_Gallery::where('status', 'PUBLISHED')->orderBy('created_at', 'desc')->take(8)->get();
        $data = array(
            'image_galleries' => $image_galleries
        );
        return view('landing-pages.index')->with($data);
    }

    public function about_me() {
        $introduction = CONFIG('about-me.introduction')['en'];

        $data = array(
            'introduction' => $introduction
        );

        return view('landing-pages.about-me')->with($data);
    }

    public function get_video($url) {
        // $url = urldecode($url);
        //$html = file_get_contents($url);
        print_r($url);
    }

    public function lost_found() {
        return view('landing-pages.lost-found');
    }
}
