<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Blog;
use Carbon\Carbon;
use URL;

class BlogController extends Controller
{
    public function index() {
        // return redirect(URL('/under-construction'));
        
        $blogs = Blog::orderBy('published_date', 'desc')->where('status', 'PUBLISHED')->paginate(10);
        $data = array(
            'blogs' => $blogs
        );
        return view('post.list')->with($data);
    }   

    public function get($canonical) {
        
        $random_id = substr($canonical, -20);
        $canonical = str_replace('-'.$random_id,'',$canonical);
        $blog = Blog::where([
            ['canonical', '=', $canonical],
            ['random_id', '=', $random_id]
        ])->first();

        // print_r($blog);

        if ($blog == null or count($blog) == 0)
            return redirect(URL('/404'));    

        $newer = Blog::where([
            ['id', '>', $blog->id],
            ['status', '=', 'PUBLISHED']
            ])->first();
        $older = Blog::where([
            ['id', '<', $blog->id],
            ['status', '=', 'PUBLISHED']
            ])->orderBy('id', 'desc')->first();

        
        $query_string = isset($_GET['edit']) ? $_GET['edit'] : '';
        if($query_string == 'yes')
            return redirect('/post-manage/'.$blog->id);

        $data = array(
            'blog' => $blog,
            'newer' => $newer,
            'older' => $older
        );
        return view('post.detail')->with($data);
    }

    public function show($id) {
        $blog = Blog::find($id);
        $data = array(
            'blog' => $blog
        );
        return view('post.detail')->with($data);
    }

    public function create() {
        return view('post.update');
    }

    public function update(Request $request, $id) {
        $blog = Blog::find($id);
        $blog->title = $request->get('title');
        $blog->canonical = toAscii($blog->title);
        $blog->content_id = $request->get('content_id');
        $blog->content_en = $request->get('content_id');
        $blog->content_jp = $request->get('content_id');
        $blog->meta_tag = $request->get('meta_tag');
        $blog->meta_description = $request->get('meta_description');
        $blog->meta_image = $request->get('meta_image');
        $blog->tag = $request->get('tag');
        $blog->status = $request->get('status');
        $blog->published_date = $request->get('published_date');
        $blog->save();
        return redirect('/post');
    }

    public function store(Request $request)
    {
        $blog = New Blog();
        $blog->title = $request->get('title');
        $blog->canonical = toAscii($blog->title);
        $blog->content_id = $request->get('content_id');
        $blog->content_en = $request->get('content_id');
        $blog->content_jp = $request->get('content_id');
        $blog->meta_tag = $request->get('meta_tag');
        $blog->meta_description = $request->get('meta_description');
        $blog->meta_image = $request->get('meta_image');
        $blog->tag = $request->get('tag');
        $blog->status = $request->get('status');
        $blog->published_date = $request->get('publised_date');
        $blog->random_id = getUUID();
        $blog->save();
        return redirect('/post');
    }

}
