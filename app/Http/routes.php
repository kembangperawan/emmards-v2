<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'LandingPageController@index');
Route::get('/archive', function () {
    return view('portfolio.list');
});
Route::get('/story', 'BlogController@index');
Route::get('/version', function () {
    return view('portfolio.list');
});
Route::get('/work', 'PortfolioController@index');
Route::get('/about-me', 'LandingPageController@about_me');

Route::get('/gallery', 'GalleryController@index');
Route::get('/camera/{canonical}', 'GalleryController@cf_index_camera');
Route::get('/gallery/{canonical}', 'GalleryController@cf_detail');


//flickr test
Route::get('/photo-info/{id}', 'GalleryController@get_flickr_exif_info');
Route::get('/test-canonical', 'GalleryController@test_canonical');
Route::get('/test-link', 'GalleryController@get_link');

Route::get('/about-me-print', function () {
    return view('landing-pages.about-me-print');
});
Route::get('/about-me-print-jp', function () {
    return view('landing-pages.about-me-print-jp');
});
Route::get('/about-me-print-rire', function () {
    return view('landing-pages.about-me-print-rire');
});
Route::get('/story/{canonical}', 'BlogController@get');
Route::get('/story-sample', function () {
    return view('post.backup');
});
Route::get('/404', function () {
    return view('other.404');
});
Route::get('/under-construction', function () {
    return view('other.under-construction');
});
Route::get('/v2', function() {
    return view('template.default-2');
});

Route::group(['middleware' => ['ServerLocation']], function () {
    Route::resource('/gallery-manage', 'GalleryController');
});
Route::get('/versi-dua', function() {
    return view('versidua.index');
});

// // nanimo
// Route::get('/kissanime-video/{url}', 'LandingPageController@get_video');
// Route::get('/benkyou/number/', 'BenkyoController@random_number');
// Route::get('/benkyou/reji/', 'BenkyoController@reji');
// Route::get('/lost-found', 'LandingPageController@lost_found');
// Route::get('/benkyou/randompet/', 'BenkyoController@randompet');