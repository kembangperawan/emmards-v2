<?php

namespace App\Http\Middleware;

use Closure;

class CheckServerLocation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ((CONFIG('app.server_location') != 'local')) {
            return redirect('/404');
        }
        return $next($request);
    }
}
