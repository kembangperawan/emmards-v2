<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogTag extends Model
{
    protected $table = 'blog_tags';
    
    function blog() {
        return $this->hasOne('App\Blog', 'blog_id')->where('status', 'PUBLISHED')->get();
    }

    function tag() {
        return $this->hasOne('App\Tag', 'tag_id')->get();
    }
}
