<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    function blog_tags() {
        return $this->hasMany('App\BlogTag', 'tag_id')->get();
    }
}
