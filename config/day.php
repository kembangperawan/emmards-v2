<?php

return [
    array(
        'en' => 'Sunday',
        'value' => 1
    ),
    array(
        'en' => 'Monday',
        'value' => 2
    ),
    array(
        'en' => 'Tuesday',
        'value' => 3
    ),
    array(
        'en' => 'Wednesday',
        'value' => 4
    ),
    array(
        'en' => 'Thursday',
        'value' => 5
    ),
    array(
        'en' => 'Friday',
        'value' => 6
    ),
    array(
        'en' => 'Saturday',
        'value' => 7
    )
];
