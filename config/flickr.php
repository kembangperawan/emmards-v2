
<?php

return [
        'app_name' => env('FLICKR_APP_NAME'),
        'app_key' => env('FLICKR_APP_KEY'),
        'app_secret' => env('FLICKR_APP_SECRET'),
        'photo_info' => [

        ],
        'photo_exif' => [
                'LensInfo',
                'LensModel',
                'FNumber',
                'ExposureTime',
                'FocalLength',
                'ISO',
                'Flash'
        ],
];
