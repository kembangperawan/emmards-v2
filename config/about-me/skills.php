<?php

return [
    array(
        'title' => 'Technical',
        'items' => [
            'Web Design : HTML, CSS/3, Javascript, PHP, VB.NET, JQuery, Mootools, Laravel, CI',
            'Mobile Apps : Swift',
            'Others : MS. SQL Server, MySQL, Microsoft Office, Photoshop'
        ]
    ),
    array(
        'title' => 'Abillities',
        'items' => [
            'Create website from scratch',
            'Create Mobile Apps iOS from scratch',
            'Transform Business Process into UML Diagram and Database Design',
            'Able to work with Scrum or Kanban Agile Methodology',
            'Create mockup design(UI/UX)',
            'Googling'
        ]
    )
];
