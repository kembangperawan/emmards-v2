
<?php

return [
    'status' => [
        "PUBLISHED",
        "UNPUBLISHED"
    ],
    'orientation' => [
        'PORTRAIT',
        'LANDSCAPE'
    ],
    'camera' => [
        array(
            'key' => 'IPHONE_7_PLUS',
            'value' => 'Apple iPhone 7 Plus',
            'canonical' => 'apple-iphone-7-plus'
        ), 
        array(
            'key' => 'SONY_ALPHA_A6000',
            'value' => 'Sony ILCE-6000',
            'canonical' => 'sony-ilce-6000'
        )
    ]
];
