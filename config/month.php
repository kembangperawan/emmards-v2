<?php

return [
    array(
        'en' => 'January',
        'value' => 1
    ),
    array(
        'en' => 'February',
        'value' => 2
    ),
    array(
        'en' => 'March',
        'value' => 3
    ),
    array(
        'en' => 'April',
        'value' => 4
    ),
    array(
        'en' => 'May',
        'value' => 5
    ),
    array(
        'en' => 'June',
        'value' => 6
    ),
    array(
        'en' => 'July',
        'value' => 7
    ),
    array(
        'en' => 'August',
        'value' => 8
    ),
    array(
        'en' => 'September',
        'value' => 9
    ),
    array(
        'en' => 'October',
        'value' => 10
    ),
    array(
        'en' => 'November',
        'value' => 11
    ),
    array(
        'en' => 'December',
        'value' => 12
    )
];
