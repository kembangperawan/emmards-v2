<?php

return [
    array(
        'code' => 'ID',
        'value' => 1,
        'title' => 'Bahasa Indonesia',
    ),
    array(
        'code' => 'EN',
        'value' => 2,
        'title' => 'English'
    ),
    array(
        'code' => 'JP',
        'value' => 3,
        'title' => '日本語'
    )
];
