<?php

return [
    array(
        'category' => 'ペット',
        'name' => '¥97',
        'price' => '97'
    ),
    array(
        'category' => 'ペット',
        'name' => '¥108',
        'price' => '108'
    ),
    array(
        'category' => 'ペット',
        'name' => '¥110',
        'price' => '110'
    ),
    array(
        'category' => 'ペット',
        'name' => '¥113',
        'price' => '113'
    ),
    array(
        'category' => 'ペット',
        'name' => '¥113',
        'price' => '113'
    ),
    array(
        'category' => 'ペット',
        'name' => '¥118',
        'price' => '118'
    ),
    array(
        'category' => 'ペット',
        'name' => '¥120',
        'price' => '120'
    ),
    array(
        'category' => 'ペット',
        'name' => '¥124',
        'price' => '124'
    ),
    array(
        'category' => 'ペット',
        'name' => '¥130',
        'price' => '130'
    ),
    array(
        'category' => 'ペット',
        'name' => '¥135',
        'price' => '135'
    ),
    array(
        'category' => 'ペット',
        'name' => '¥136',
        'price' => '136'
    ),
    array(
        'category' => 'ペット',
        'name' => '¥140',
        'price' => '140'
    ),
    array(
        'category' => 'ペット',
        'name' => '¥150',
        'price' => '150'
    ),
    array(
        'category' => 'ペット',
        'name' => '¥151',
        'price' => '151'
    ),
    array(
        'category' => 'ペット',
        'name' => '¥157',
        'price' => '157'
    ),
    array(
        'category' => 'ペット',
        'name' => '¥162',
        'price' => '162'
    ),
    array(
        'category' => 'ペット',
        'name' => '¥172',
        'price' => '172'
    ),
    array(
        'category' => 'ペット',
        'name' => '¥183',
        'price' => '183'
    ),
    array(
        'category' => 'ペット',
        'name' => '¥194',
        'price' => '194'
    ),
    array(
        'category' => 'ペット',
        'name' => '¥216',
        'price' => '216'
    ),
    array(
        'category' => 'フード',
        'name' => 'ライス',
        'price' => '115'
    ),
    array(
        'category' => 'フード',
        'name' => 'なつかしのクロッケ',
        'price' => '115'
    ),
    array(
        'category' => 'フード',
        'name' => '牛メンチカツサンド',
        'price' => '115'
    ),
    array(
        'category' => 'フード',
        'name' => 'スープ108',
        'price' => '108'
    ),
    array(
        'category' => 'フード',
        'name' => 'おにぎり',
        'price' => '108'
    ),
    array(
        'category' => 'フード',
        'name' => 'ハムエッグサンド',
        'price' => '108'
    ),
    array(
        'category' => 'フード',
        'name' => 'チキンカツサンド',
        'price' => '108'
    ),
    array(
        'category' => 'フード',
        'name' => 'スープ118',
        'price' => '108'
    ),
    array(
        'category' => 'フード',
        'name' => '弁当３００',
        'price' => '300'
    ),
    array(
        'category' => 'フード',
        'name' => 'ミックスサンド',
        'price' => '108'
    ),
    array(
        'category' => 'フード',
        'name' => 'ハムトマト',
        'price' => '108'
    ),
    array(
        'category' => 'フード',
        'name' => 'スープ140',
        'price' => '140'
    ),
    array(
        'category' => 'フード',
        'name' => '弁当400',
        'price' => '400'
    ),
    array(
        'category' => 'フード',
        'name' => 'ライ麦チーズ',
        'price' => '108'
    ),
    array(
        'category' => 'フード',
        'name' => 'デザート136',
        'price' => '136'
    ),
    array(
        'category' => 'フード',
        'name' => 'スープ162',
        'price' => '162'
    ),
    array(
        'category' => 'フード',
        'name' => '弁当500',
        'price' => '500'
    ),
    array(
        'category' => 'フード',
        'name' => 'ヒレカツサンド',
        'price' => '136'
    ),
    array(
        'category' => 'フード',
        'name' => 'スープ140',
        'price' => '140'
    ),
    array(
        'category' => 'フード',
        'name' => 'スープ172',
        'price' => '172'
    ),
    array(
        'category' => 'お菓子',
        'name' => '小魚45',
        'price' => '45'
    ),
    array(
        'category' => 'お菓子',
        'name' => 'お菓子76',
        'price' => '76'
    ),
    array(
        'category' => 'お菓子',
        'name' => 'お菓子108',
        'price' => '108'
    ),
    array(
        'category' => 'お菓子',
        'name' => 'お菓子119',
        'price' => '119'
    ),
    array(
        'category' => 'お菓子',
        'name' => 'お菓子129',
        'price' => '129'
    ),
    array(
        'category' => 'お菓子',
        'name' => 'お菓子130',
        'price' => '130'
    ),
    array(
        'category' => 'お菓子',
        'name' => 'お菓子140',
        'price' => '140'
    ),
    array(
        'category' => 'お菓子',
        'name' => 'お菓子151',
        'price' => '151'
    ),
    array(
        'category' => 'お菓子',
        'name' => 'お菓子162',
        'price' => '162'
    ),
    array(
        'category' => 'お菓子',
        'name' => 'お菓子173',
        'price' => '173'
    ),
    array(
        'category' => 'お菓子',
        'name' => 'お菓子177',
        'price' => '177'
    ),
    array(
        'category' => 'お菓子',
        'name' => 'お菓子194',
        'price' => '194'
    ),
    array(
        'category' => 'お菓子',
        'name' => 'お菓子200',
        'price' => '200'
    ),
    array(
        'category' => 'お菓子',
        'name' => 'お菓子210',
        'price' => '210'
    ),
    array(
        'category' => 'お菓子',
        'name' => 'お菓子238',
        'price' => '238'
    ),
    array(
        'category' => 'お菓子',
        'name' => 'お菓子270',
        'price' => '270'
    ),
    array(
        'category' => 'お菓子',
        'name' => 'お菓子325',
        'price' => '325'
    ),
    array(
        'category' => 'お菓子',
        'name' => 'お菓子335',
        'price' => '335'
    ),
    array(
        'category' => 'お菓子',
        'name' => 'お菓子350',
        'price' => '350'
    ),
    array(
        'category' => 'お菓子',
        'name' => 'お菓子380',
        'price' => '380'
    ),
    array(
        'category' => '惣菜パン',
        'name' => '塩バターパン',
        'price' => '380'
    ),
    array(
        'category' => '惣菜パン',
        'name' => 'フランスピロシキ',
        'price' => '380'
    ),
    array(
        'category' => '惣菜パン',
        'name' => 'ウインナーロール',
        'price' => '380'
    ),
    array(
        'category' => '惣菜パン',
        'name' => 'フランクロール',
        'price' => '380'
    ),
    array(
        'category' => '惣菜パン',
        'name' => 'ハムチーズデニス',
        'price' => '380'
    ),
    array(
        'category' => '惣菜パン',
        'name' => '焼きそばパン',
        'price' => '380'
    ),
    array(
        'category' => '惣菜パン',
        'name' => 'ツナタマゴクッペ',
        'price' => '380'
    ),
    array(
        'category' => '惣菜パン',
        'name' => 'ベーコンエッグ',
        'price' => '380'
    ),
    array(
        'category' => '惣菜パン',
        'name' => 'メンチカツカレー',
        'price' => '380'
    ),
    array(
        'category' => '惣菜パン',
        'name' => '日替ピザ',
        'price' => '380'
    ),
    array(
        'category' => '惣菜パン',
        'name' => 'アスパラと菜の花',
        'price' => '380'
    ),
    array(
        'category' => '惣菜パン',
        'name' => 'バジルフォカチャ',
        'price' => '380'
    ),
    array(
        'category' => '惣菜パン',
        'name' => 'ツナマヨピザフランス',
        'price' => '380'
    ),
    array(
        'category' => '惣菜パン',
        'name' => 'じゃがいも明太',
        'price' => '380'
    ),
    array(
        'category' => '惣菜パン',
        'name' => 'グラタンコロッケパン',
        'price' => '380'
    ),
    array(
        'category' => '惣菜パン',
        'name' => 'そら豆ベーコン',
        'price' => '380'
    ),
    array(
        'category' => '惣菜パン',
        'name' => 'ハムエッグロール',
        'price' => '380'
    ),
    array(
        'category' => '惣菜パン',
        'name' => 'バジルと完熟トマト',
        'price' => '380'
    ),
    array(
        'category' => '惣菜パン',
        'name' => '明太チーズ',
        'price' => '380'
    ),
    array(
        'category' => '惣菜パン',
        'name' => 'ピザポテト',
        'price' => '380'
    ),
    array(
        'category' => '菓子パン',
        'name' => 'ホイップ小倉',
        'price' => '380'
    ),
    array(
        'category' => '菓子パン',
        'name' => 'ホイップメロン',
        'price' => '380'
    ),
    array(
        'category' => '菓子パン',
        'name' => 'ベーグル',
        'price' => '380'
    ),
    array(
        'category' => '菓子パン',
        'name' => 'ショコラパネトーネ',
        'price' => '380'
    ),
    array(
        'category' => '菓子パン',
        'name' => 'ソフトレーズン',
        'price' => '380'
    ),
    array(
        'category' => '菓子パン',
        'name' => '蒸しパン',
        'price' => '380'
    ),
    array(
        'category' => '菓子パン',
        'name' => '-',
        'price' => '380'
    ),
    array(
        'category' => '菓子パン',
        'name' => '紅茶スコーン',
        'price' => '380'
    ),
    array(
        'category' => '菓子パン',
        'name' => '-',
        'price' => '380'
    ),
    array(
        'category' => '菓子パン',
        'name' => '林檎クリームデニス',
        'price' => '380'
    ),
    array(
        'category' => '菓子パン',
        'name' => 'チョコクロワッサン',
        'price' => '380'
    ),
    array(
        'category' => '菓子パン',
        'name' => '高級デニッシュ',
        'price' => '380'
    ),
    array(
        'category' => '菓子パン',
        'name' => '-',
        'price' => '380'
    ),
    array(
        'category' => '菓子パン',
        'name' => 'うずまきキャラメル',
        'price' => '380'
    ),
    array(
        'category' => '菓子パン',
        'name' => 'カフェモカデニッシュ',
        'price' => '380'
    ),
    array(
        'category' => '菓子パン',
        'name' => 'バーターロール',
        'price' => '380'
    ),
    array(
        'category' => '菓子パン',
        'name' => 'タルト',
        'price' => '380'
    ),
    array(
        'category' => '菓子パン',
        'name' => 'ハムチーズデニッシュ',
        'price' => '380'
    ),
    array(
        'category' => '菓子パン',
        'name' => 'ミニクロワッサン',
        'price' => '380'
    ),
    array(
        'category' => '菓子パン',
        'name' => '甘夏とレモン',
        'price' => '380'
    ),
    array(
        'category' => '場げ物',
        'name' => 'オールドファッション',
        'price' => '380'
    ),
    array(
        'category' => '場げ物',
        'name' => '原宿ドッグ',
        'price' => '380'
    ),
    array(
        'category' => '場げ物',
        'name' => 'あんドーナツ',
        'price' => '380'
    ),
    array(
        'category' => '場げ物',
        'name' => 'カレードーナツ',
        'price' => '380'
    ),
    array(
        'category' => '場げ物',
        'name' => 'ヨーロピアン',
        'price' => '380'
    ),
    array(
        'category' => '場げ物',
        'name' => 'チーズミート',
        'price' => '380'
    ),
    array(
        'category' => '場げ物',
        'name' => '安納芋ドーナツ',
        'price' => '380'
    ),
    array(
        'category' => '場げ物',
        'name' => 'アメリカンドッグ',
        'price' => '380'
    ),
    array(
        'category' => '場げ物',
        'name' => 'ハッシュドポテト',
        'price' => '380'
    ),
    array(
        'category' => '場げ物',
        'name' => 'そら豆ベーコン',
        'price' => '380'
    ),
    array(
        'category' => '場げ物',
        'name' => 'てりマヨチキン',
        'price' => '380'
    ),
    array(
        'category' => '場げ物',
        'name' => 'メンチカツカレー',
        'price' => '380'
    ),
    array(
        'category' => '場げ物',
        'name' => 'クリームブリオッシュ',
        'price' => '380'
    ),
    array(
        'category' => '場げ物',
        'name' => 'アスパラと菜の花',
        'price' => '380'
    ),
    array(
        'category' => '場げ物',
        'name' => 'ごぼうサラダパン',
        'price' => '380'
    ),
    array(
        'category' => '場げ物',
        'name' => 'ベーコンエピ',
        'price' => '380'
    ),
    array(
        'category' => '場げ物',
        'name' => 'マカロニウインナー',
        'price' => '380'
    ),
    array(
        'category' => '場げ物',
        'name' => 'サワークリームベーコン',
        'price' => '380'
    ),
    array(
        'category' => '場げ物',
        'name' => 'ー',
        'price' => '380'
    ),
    array(
        'category' => '場げ物',
        'name' => 'ピリ辛タコスドッグ',
        'price' => '380'
    ),
];
